unit OpcClient;

interface

uses
  Variants, Windows, Forms, ComObj, ActiveX, SysUtils, OPCtypes, OPCDA, OPCutils,
  Classes, TagValueUtils;

const
  OneSecond = 1 ;
  RPC_C_AUTHN_LEVEL_NONE = 1;
  RPC_C_IMP_LEVEL_IMPERSONATE = 3;
  EOAC_NONE = 0;
  OPC_GROUP = 'group';

type

  TOPCAdviseSink = class(TInterfacedObject, IAdviseSink)
  public
    procedure OnDataChange(const formatetc: TFormatEtc;
                            const stgmed: TStgMedium); stdcall;
    procedure OnViewChange(dwAspect: Longint; lindex: Longint); stdcall;
    procedure OnRename(const mk: IMoniker); stdcall;
    procedure OnSave; stdcall;
    procedure OnClose; stdcall;
  end;

  // class to receive IConnectionPointContainer data change callbacks
  TOPCDataCallback = class(TInterfacedObject, IOPCDataCallback)
  public
    function OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
      pErrors: PResultList): HResult; stdcall;
    function OnCancelComplete(dwTransid: DWORD; hGroup: OPCHANDLE):
      HResult; stdcall;
  end;

  TOPCClient = class
  private
//    FOPCServer: OPCServer;
    FOPCOleName: string;
    FOPCTagList: TStrings;
    ServerIf: IOPCServer;
    GroupIf: IOPCItemMgt;
    GroupHandle: OPCHANDLE;
    //ItemValue: string;
    //ItemQuality: Word;
    HR: HResult;
    AdviseSink: IAdviseSink;
    AsyncConnection: Longint;
    OPCDataCallback: IOPCDataCallback;
    StartTime: TDateTime;
    FOPCTagHandleList: TStrings;
    FStop: Boolean;
    function InitDCOM: Boolean;
    function ConnectToOleServer: Boolean;
    procedure AddGroups;
    procedure AddAllTags;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Init;
    procedure AddTag(ATagName: string);
    function ReadTagValue(ATagName: string; var Value,
      Quality, TimeStamp: OleVariant): Boolean;
    function ReadAllTagValue(var ValueList: TRxList): string;
    procedure GetAllTag(TagList: TStrings);
    procedure StartReadValue;
    procedure Stop;
  published
    property OPCOleName: string read FOPCOleName write FOPCOleName;
    property OPCTagList: TStrings read FOPCTagList;
  end;

procedure WriteMessage(msg: String);

implementation

uses
  unit1;

procedure WriteMessage(msg: String);
begin
  Form1.Memo1.Lines.Add(msg);
end;

{ TOPCDataCallback }

function TOPCDataCallback.OnCancelComplete(dwTransid: DWORD;
  hGroup: OPCHANDLE): HResult;
begin

end;

function TOPCDataCallback.OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality, hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
var
  ClientItems: POPCHANDLEARRAY;
  Values: POleVariantArray;
  Qualities: PWORDARRAY;
  I: Integer;
  NewValue: string;
begin
  Result := S_OK;
  ClientItems := POPCHANDLEARRAY(phClientItems);
  Values := POleVariantArray(pvValues);
  Qualities := PWORDARRAY(pwQualities);
  for I := 0 to dwCount - 1 do
  begin
    if Qualities[I] = OPC_QUALITY_GOOD then
    begin
      NewValue := VarToStr(Values[I]);
      WriteMessage('New callback for item ' +IntToStr(ClientItems[I]) + ' received, value:  ' +
              NewValue);
    end
    else begin
      writeln('Callback received for item ' + IntToStr(ClientItems[I]) +
              ' , but quality not good');
    end;
  end;
end;

function TOPCDataCallback.OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality, hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
begin
  Result := OnDataChange(dwTransid, hGroup, hrMasterquality, hrMastererror,
    dwCount, phClientItems, pvValues, pwQualities, pftTimeStamps, pErrors);
end;

function TOPCDataCallback.OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
  pErrors: PResultList): HResult;
begin
  Result := S_OK;
end;

{ TOPCAdviseSink }

procedure TOPCAdviseSink.OnClose;
begin

end;

procedure TOPCAdviseSink.OnDataChange(const formatetc: TFormatEtc;
  const stgmed: TStgMedium);
var
  PG: POPCGROUPHEADER;
  PI1: POPCITEMHEADER1ARRAY;
  PI2: POPCITEMHEADER2ARRAY;
  PV: POleVariant;
  I: Integer;
  PStr: PWideChar;
  NewValue: string;
  WithTime: Boolean;
  ClientHandle: OPCHANDLE;
  Quality: Word;
begin
  // the rest of this method assumes that the item header array uses
  // OPCITEMHEADER1 or OPCITEMHEADER2 records,
  // so check this first to be defensive
  if (formatetc.cfFormat <> OPCSTMFORMATDATA) and
      (formatetc.cfFormat <> OPCSTMFORMATDATATIME) then Exit;
  // does the data stream provide timestamps with each value?
  WithTime := formatetc.cfFormat = OPCSTMFORMATDATATIME;
  PG := GlobalLock(stgmed.hGlobal);
  if PG <> nil then
  begin
    // we will only use one of these two values, according to whether
    // WithTime is set:
    PI1 := Pointer(PAnsiChar(PG) + SizeOf(OPCGROUPHEADER));
    PI2 := Pointer(PI1);
    if Succeeded(PG.hrStatus) then
    begin
      for I := 0 to PG.dwItemCount - 1 do
      begin
        if WithTime then
        begin
          PV := POleVariant(PAnsiChar(PG) + PI1[I].dwValueOffset);
          ClientHandle := PI1[I].hClient;
          Quality := (PI1[I].wQuality and OPC_QUALITY_MASK);
        end
        else begin
          PV := POleVariant(PAnsiChar(PG) + PI2[I].dwValueOffset);
          ClientHandle := PI2[I].hClient;
          Quality := (PI2[I].wQuality and OPC_QUALITY_MASK);
        end;
        if Quality = OPC_QUALITY_GOOD then
        begin
          // this test assumes we're not dealing with array data
          if TVarData(PV^).VType <> VT_BSTR then
          begin
            NewValue := VarToStr(PV^);
          end
          else begin
            // for BSTR data, the BSTR image follows immediately in the data
            // stream after the variant union;  the BSTR begins with a DWORD
            // character count, which we skip over as the BSTR is also
            // NULL-terminated
            PStr := PWideChar(PAnsiChar(PV) + SizeOf(OleVariant) + 4);
            NewValue := WideString(PStr);
          end;
          if WithTime then
          begin
            writeln(FormatDateTime('YYYY-MM-DD hh:nn:ss ZZZ',now), '  New value for item ', ClientHandle, ' advised:  ',
                    NewValue, ' (with timestamp)');
          end
          else begin
            writeln(FormatDateTime('YYYY-MM-DD hh:nn:ss ZZZ',now), '  New value for item ', ClientHandle, ' advised:  ',
                    NewValue);
          end;
        end
        else begin
          writeln('Advise received for item ', ClientHandle,
                  ' , but quality not good');
        end;
      end;
    end;
    GlobalUnlock(stgmed.hGlobal);
  end;
end;

procedure TOPCAdviseSink.OnRename(const mk: IMoniker);
begin

end;

procedure TOPCAdviseSink.OnSave;
begin

end;

procedure TOPCAdviseSink.OnViewChange(dwAspect, lindex: Longint);
begin

end;

{ TOPCClient }

procedure TOPCClient.AddAllTags;
var
  I: Integer;
  TagHandle: OPCHANDLE;
  ItemType: TVarType;
begin
  //将点添加到组中
  for I := 0 to OPCTagList.Count - 1 do
  begin
    HR := GroupAddItem(GroupIf, OPCTagList[I], I, VT_EMPTY, TagHandle, ItemType);
    if Succeeded(HR) then
    begin
      WriteMessage('Added item'+inttostr(i)+' to group');
    end
    else begin
      WriteMessage('Unable to add item '+inttostr(i)+' to group' + OPCTagList[I]);
      ServerIf.RemoveGroup(GroupHandle, False);
      Exit;
    end;
  end;
end;

procedure TOPCClient.AddGroups;
begin
  //添加组信息
  HR := ServerAddGroup(ServerIf, OPC_GROUP, True, 10, 0, GroupIf, GroupHandle);
  if Succeeded(HR) then
  begin
    WriteMessage('Added group to server');
  end
  else begin
    WriteMessage('Unable to add group to server');
    Exit;
  end;
end;

procedure TOPCClient.AddTag(ATagName: string);
begin
  if OPCTagList.IndexOf(ATagName) = -1 then
    OPCTagList.Add(ATagName);
end;

function TOPCClient.ConnectToOleServer: Boolean;
begin
  try
    // we will use the custom OPC interfaces, and OPCProxy.dll will handle
    // marshaling for us automatically (if registered)
    ServerIf := CreateComObject(ProgIDToClassID(FOPCOleName)) as IOPCServer;
  except
    ServerIf := nil;
  end;
  if ServerIf <> nil then
  begin
    writemessage('Connected to OPC server');
    Result := True;
  end
  else begin
    writemessage('Unable to connect to OPC server');
    Exit;
  end;
end;

constructor TOPCClient.Create;
begin
  FOPCTagList := TStringList.Create;
  FOPCTagHandleList := TStringList.Create;
end;

destructor TOPCClient.Destroy;
begin
  FreeAndNil(FOPCTagHandleList);
  FreeAndNil(FOPCTagList);
  inherited;
end;

procedure TOPCClient.GetAllTag(TagList: TStrings);
begin

end;

procedure TOPCClient.Init;
begin
  if ConnectToOleServer then
  begin
    //添加组
    AddGroups;
    AddAllTags;
  end;
end;

function TOPCClient.InitDCOM: Boolean;
begin
    HR := CoInitializeSecurity(
    nil,                    // points to security descriptor
    -1,                     // count of entries in asAuthSvc
    nil,                    // array of names to register
    nil,                    // reserved for future use
    RPC_C_AUTHN_LEVEL_NONE, // the default authentication level for proxies
    RPC_C_IMP_LEVEL_IMPERSONATE,// the default impersonation level for proxies
    nil,                    // used only on Windows 2000
    EOAC_NONE,              // additional client or server-side capabilities
    nil                     // reserved for future use
    );
  if Failed(HR) then
  begin
    WriteMessage('Failed to initialize DCOM security');
  end;
  Result := Failed(HR);
end;

function TOPCClient.ReadAllTagValue(var ValueList: TRxList): string;
begin

end;

function TOPCClient.ReadTagValue(ATagName: string; var Value, Quality,
  TimeStamp: OleVariant): Boolean;
begin

end;

procedure TOPCClient.StartReadValue;
begin
  OPCDataCallback := TOPCDataCallback.Create;
  HR := GroupAdvise2(GroupIf, OPCDataCallback, AsyncConnection);
  if Failed(HR) then
  begin
    WriteMessage('Failed to set up IConnectionPointContainer advise callback');
  end
  else begin
    WriteMessage('IConnectionPointContainer data callback established');
    // continue waiting for callbacks for 10 seconds
    StartTime := Now;
    while not FStop do
    begin
      Application.ProcessMessages;
    end;
    // end the IConnectionPointContainer data callback
    GroupUnadvise2(GroupIf, AsyncConnection);
  end;

end;

procedure TOPCClient.Stop;
begin
  FStop := True;
end;

end.

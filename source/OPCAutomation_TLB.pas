unit OPCAutomation_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 2019/9/30 17:13:59 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Windows\SysWow64\OPCDAAuto.dll (1)
// LIBID: {28E68F91-8D75-11D1-8DC3-3C302A000000}
// LCID: 0
// Helpfile:
// HelpString: OPC DA Automation Wrapper 2.02
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Windows, Classes, Variants, StdVCL, Graphics, OleServer, ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  OPCAutomationMajorVersion = 1;
  OPCAutomationMinorVersion = 0;

  LIBID_OPCAutomation: TGUID = '{28E68F91-8D75-11D1-8DC3-3C302A000000}';

  IID_IOPCAutoServer: TGUID = '{28E68F92-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCGroups: TGUID = '{28E68F95-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCGroupsEvent: TGUID = '{28E68F9D-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCGroup: TGUID = '{28E68F96-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCGroupEvent: TGUID = '{28E68F97-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCItems: TGUID = '{28E68F98-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCItem: TGUID = '{28E68F99-8D75-11D1-8DC3-3C302A000000}';
  CLASS_OPCGroup: TGUID = '{28E68F9B-8D75-11D1-8DC3-3C302A000000}';
  CLASS_OPCGroups: TGUID = '{28E68F9E-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCBrowser: TGUID = '{28E68F94-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCServerEvent: TGUID = '{28E68F93-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCActivator: TGUID = '{860A4800-46A4-478B-A776-7F3A019369E3}';
  CLASS_OPCActivator: TGUID = '{860A4801-46A4-478B-A776-7F3A019369E3}';
  CLASS_OPCServer: TGUID = '{28E68F9A-8D75-11D1-8DC3-3C302A000000}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library
// *********************************************************************//
// Constants for enum OPCNamespaceTypes
type
  OPCNamespaceTypes = TOleEnum;
const
  OPCHierarchical = $00000001;
  OPCFlat = $00000002;

// Constants for enum OPCDataSource
type
  OPCDataSource = TOleEnum;
const
  OPCCache = $00000001;
  OPCDevice = $00000002;

// Constants for enum OPCAccessRights
type
  OPCAccessRights = TOleEnum;
const
  OPCReadable = $00000001;
  OPCWritable = $00000002;

// Constants for enum OPCServerState
type
  OPCServerState = TOleEnum;
const
  OPCRunning = $00000001;
  OPCFailed = $00000002;
  OPCNoconfig = $00000003;
  OPCSuspended = $00000004;
  OPCTest = $00000005;
  OPCDisconnected = $00000006;

// Constants for enum OPCErrors
type
  OPCErrors = TOleEnum;
const
  OPCInvalidHandle = $C0040001;
  OPCBadType = $C0040004;
  OPCPublic = $C0040005;
  OPCBadRights = $C0040006;
  OPCUnknownItemID = $C0040007;
  OPCInvalidItemID = $C0040008;
  OPCInvalidFilter = $C0040009;
  OPCUnknownPath = $C004000A;
  OPCRange = $C004000B;
  OPCDuplicateName = $C004000C;
  OPCUnsupportedRate = $0004000D;
  OPCClamp = $0004000E;
  OPCInuse = $0004000F;
  OPCInvalidConfig = $C0040010;
  OPCNotFound = $C0040011;
  OPCInvalidPID = $C0040203;

// Constants for enum OPCQuality
type
  OPCQuality = TOleEnum;
const
  OPCQualityMask = $000000C0;
  OPCQualityBad = $00000000;
  OPCQualityUncertain = $00000040;
  OPCQualityGood = $000000C0;

// Constants for enum OPCQualityStatus
type
  OPCQualityStatus = TOleEnum;
const
  OPCStatusMask = $000000FC;
  OPCStatusConfigError = $00000004;
  OPCStatusNotConnected = $00000008;
  OPCStatusDeviceFailure = $0000000C;
  OPCStatusSensorFailure = $00000010;
  OPCStatusLastKnown = $00000014;
  OPCStatusCommFailure = $00000018;
  OPCStatusOutOfService = $0000001C;
  OPCStatusLastUsable = $00000044;
  OPCStatusSensorCal = $00000050;
  OPCStatusEGUExceeded = $00000054;
  OPCStatusSubNormal = $00000058;
  OPCStatusLocalOverride = $000000D8;

// Constants for enum OPCQualityLimits
type
  OPCQualityLimits = TOleEnum;
const
  OPCLimitMask = $00000003;
  OPCLimitOk = $00000000;
  OPCLimitLow = $00000001;
  OPCLimitHigh = $00000002;
  OPCLimitConst = $00000003;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IOPCAutoServer = interface;
  IOPCAutoServerDisp = dispinterface;
  IOPCGroups = interface;
  IOPCGroupsDisp = dispinterface;
  DIOPCGroupsEvent = dispinterface;
  IOPCGroup = interface;
  IOPCGroupDisp = dispinterface;
  DIOPCGroupEvent = dispinterface;
  OPCItems = interface;
  OPCItemsDisp = dispinterface;
  OPCItem = interface;
  OPCItemDisp = dispinterface;
  OPCBrowser = interface;
  OPCBrowserDisp = dispinterface;
  DIOPCServerEvent = dispinterface;
  IOPCActivator = interface;
  IOPCActivatorDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  OPCGroup = IOPCGroup;
  OPCGroups = IOPCGroups;
  OPCActivator = IOPCActivator;
  OPCServer = IOPCAutoServer;


// *********************************************************************//
// Declaration of structures, unions and aliases.
// *********************************************************************//
  PPSafeArray1 = ^PSafeArray; {*}
  PPSafeArray2 = ^PSafeArray; {*}
  PPSafeArray3 = ^PSafeArray; {*}
  PPSafeArray4 = ^PSafeArray; {*}


// *********************************************************************//
// Interface: IOPCAutoServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F92-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCAutoServer = interface(IDispatch)
    ['{28E68F92-8D75-11D1-8DC3-3C302A000000}']
    function Get_StartTime: TDateTime; safecall;
    function Get_CurrentTime: TDateTime; safecall;
    function Get_LastUpdateTime: TDateTime; safecall;
    function Get_MajorVersion: Smallint; safecall;
    function Get_MinorVersion: Smallint; safecall;
    function Get_BuildNumber: Smallint; safecall;
    function Get_VendorInfo: WideString; safecall;
    function Get_ServerState: Integer; safecall;
    function Get_ServerName: WideString; safecall;
    function Get_ServerNode: WideString; safecall;
    function Get_ClientName: WideString; safecall;
    procedure Set_ClientName(const ClientName: WideString); safecall;
    function Get_LocaleID: Integer; safecall;
    procedure Set_LocaleID(LocaleID: Integer); safecall;
    function Get_Bandwidth: Integer; safecall;
    function Get_OPCGroups: OPCGroups; safecall;
    function Get_PublicGroupNames: OleVariant; safecall;
    function GetOPCServers(Node: OleVariant): OleVariant; safecall;
    procedure Connect(const ProgID: WideString; Node: OleVariant); safecall;
    procedure Disconnect; safecall;
    function CreateBrowser: OPCBrowser; safecall;
    function GetErrorString(ErrorCode: Integer): WideString; safecall;
    function QueryAvailableLocaleIDs: OleVariant; safecall;
    procedure QueryAvailableProperties(const ItemID: WideString; out Count: Integer;
                                       out PropertyIDs: PSafeArray; out Descriptions: PSafeArray;
                                       out DataTypes: PSafeArray); safecall;
    procedure GetItemProperties(const ItemID: WideString; Count: Integer;
                                var PropertyIDs: PSafeArray; out PropertyValues: PSafeArray;
                                out Errors: PSafeArray); safecall;
    procedure LookupItemIDs(const ItemID: WideString; Count: Integer; var PropertyIDs: PSafeArray;
                            out NewItemIDs: PSafeArray; out Errors: PSafeArray); safecall;
    property StartTime: TDateTime read Get_StartTime;
    property CurrentTime: TDateTime read Get_CurrentTime;
    property LastUpdateTime: TDateTime read Get_LastUpdateTime;
    property MajorVersion: Smallint read Get_MajorVersion;
    property MinorVersion: Smallint read Get_MinorVersion;
    property BuildNumber: Smallint read Get_BuildNumber;
    property VendorInfo: WideString read Get_VendorInfo;
    property ServerState: Integer read Get_ServerState;
    property ServerName: WideString read Get_ServerName;
    property ServerNode: WideString read Get_ServerNode;
    property ClientName: WideString read Get_ClientName write Set_ClientName;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property Bandwidth: Integer read Get_Bandwidth;
    property OPCGroups: OPCGroups read Get_OPCGroups;
    property PublicGroupNames: OleVariant read Get_PublicGroupNames;
  end;

// *********************************************************************//
// DispIntf:  IOPCAutoServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F92-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCAutoServerDisp = dispinterface
    ['{28E68F92-8D75-11D1-8DC3-3C302A000000}']
    property StartTime: TDateTime readonly dispid 1610743808;
    property CurrentTime: TDateTime readonly dispid 1610743809;
    property LastUpdateTime: TDateTime readonly dispid 1610743810;
    property MajorVersion: Smallint readonly dispid 1610743811;
    property MinorVersion: Smallint readonly dispid 1610743812;
    property BuildNumber: Smallint readonly dispid 1610743813;
    property VendorInfo: WideString readonly dispid 1610743814;
    property ServerState: Integer readonly dispid 1610743815;
    property ServerName: WideString readonly dispid 1610743816;
    property ServerNode: WideString readonly dispid 1610743817;
    property ClientName: WideString dispid 1610743818;
    property LocaleID: Integer dispid 1610743820;
    property Bandwidth: Integer readonly dispid 1610743822;
    property OPCGroups: OPCGroups readonly dispid 0;
    property PublicGroupNames: OleVariant readonly dispid 1610743824;
    function GetOPCServers(Node: OleVariant): OleVariant; dispid 1610743825;
    procedure Connect(const ProgID: WideString; Node: OleVariant); dispid 1610743826;
    procedure Disconnect; dispid 1610743827;
    function CreateBrowser: OPCBrowser; dispid 1610743828;
    function GetErrorString(ErrorCode: Integer): WideString; dispid 1610743829;
    function QueryAvailableLocaleIDs: OleVariant; dispid 1610743830;
    procedure QueryAvailableProperties(const ItemID: WideString; out Count: Integer;
                                       out PropertyIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                       out Descriptions: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                       out DataTypes: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743831;
    procedure GetItemProperties(const ItemID: WideString; Count: Integer;
                                var PropertyIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                out PropertyValues: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743832;
    procedure LookupItemIDs(const ItemID: WideString; Count: Integer;
                            var PropertyIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                            out NewItemIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                            out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743833;
  end;

// *********************************************************************//
// Interface: IOPCGroups
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F95-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroups = interface(IDispatch)
    ['{28E68F95-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: IOPCAutoServer; safecall;
    function Get_DefaultGroupIsActive: WordBool; safecall;
    procedure Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool); safecall;
    function Get_DefaultGroupUpdateRate: Integer; safecall;
    procedure Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer); safecall;
    function Get_DefaultGroupDeadband: Single; safecall;
    procedure Set_DefaultGroupDeadband(DefaultGroupDeadband: Single); safecall;
    function Get_DefaultGroupLocaleID: Integer; safecall;
    procedure Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer); safecall;
    function Get_DefaultGroupTimeBias: Integer; safecall;
    procedure Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): OPCGroup; safecall;
    function Add(Name: OleVariant): OPCGroup; safecall;
    function GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup; safecall;
    procedure RemoveAll; safecall;
    procedure Remove(ItemSpecifier: OleVariant); safecall;
    function ConnectPublicGroup(const Name: WideString): OPCGroup; safecall;
    procedure RemovePublicGroup(ItemSpecifier: OleVariant); safecall;
    property Parent: IOPCAutoServer read Get_Parent;
    property DefaultGroupIsActive: WordBool read Get_DefaultGroupIsActive write Set_DefaultGroupIsActive;
    property DefaultGroupUpdateRate: Integer read Get_DefaultGroupUpdateRate write Set_DefaultGroupUpdateRate;
    property DefaultGroupDeadband: Single read Get_DefaultGroupDeadband write Set_DefaultGroupDeadband;
    property DefaultGroupLocaleID: Integer read Get_DefaultGroupLocaleID write Set_DefaultGroupLocaleID;
    property DefaultGroupTimeBias: Integer read Get_DefaultGroupTimeBias write Set_DefaultGroupTimeBias;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  IOPCGroupsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F95-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroupsDisp = dispinterface
    ['{28E68F95-8D75-11D1-8DC3-3C302A000000}']
    property Parent: IOPCAutoServer readonly dispid 1610743808;
    property DefaultGroupIsActive: WordBool dispid 1610743809;
    property DefaultGroupUpdateRate: Integer dispid 1610743811;
    property DefaultGroupDeadband: Single dispid 1610743813;
    property DefaultGroupLocaleID: Integer dispid 1610743815;
    property DefaultGroupTimeBias: Integer dispid 1610743817;
    property Count: Integer readonly dispid 1610743819;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): OPCGroup; dispid 0;
    function Add(Name: OleVariant): OPCGroup; dispid 1610743822;
    function GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup; dispid 1610743823;
    procedure RemoveAll; dispid 1610743824;
    procedure Remove(ItemSpecifier: OleVariant); dispid 1610743825;
    function ConnectPublicGroup(const Name: WideString): OPCGroup; dispid 1610743826;
    procedure RemovePublicGroup(ItemSpecifier: OleVariant); dispid 1610743827;
  end;

// *********************************************************************//
// DispIntf:  DIOPCGroupsEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F9D-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCGroupsEvent = dispinterface
    ['{28E68F9D-8D75-11D1-8DC3-3C302A000000}']
    procedure GlobalDataChange(TransactionID: Integer; GroupHandle: Integer; NumItems: Integer;
                               var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                               var ItemValues: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                               var Qualities: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                               var TimeStamps: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1;
  end;

// *********************************************************************//
// Interface: IOPCGroup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F96-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroup = interface(IDispatch)
    ['{28E68F96-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: IOPCAutoServer; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Name: WideString); safecall;
    function Get_IsPublic: WordBool; safecall;
    function Get_IsActive: WordBool; safecall;
    procedure Set_IsActive(IsActive: WordBool); safecall;
    function Get_IsSubscribed: WordBool; safecall;
    procedure Set_IsSubscribed(IsSubscribed: WordBool); safecall;
    function Get_ClientHandle: Integer; safecall;
    procedure Set_ClientHandle(ClientHandle: Integer); safecall;
    function Get_ServerHandle: Integer; safecall;
    function Get_LocaleID: Integer; safecall;
    procedure Set_LocaleID(LocaleID: Integer); safecall;
    function Get_TimeBias: Integer; safecall;
    procedure Set_TimeBias(TimeBias: Integer); safecall;
    function Get_DeadBand: Single; safecall;
    procedure Set_DeadBand(DeadBand: Single); safecall;
    function Get_UpdateRate: Integer; safecall;
    procedure Set_UpdateRate(UpdateRate: Integer); safecall;
    function Get_OPCItems: OPCItems; safecall;
    procedure SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray;
                       out Values: PSafeArray; out Errors: PSafeArray; out Qualities: OleVariant;
                       out TimeStamps: OleVariant); safecall;
    procedure SyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray;
                        out Errors: PSafeArray); safecall;
    procedure AsyncRead(NumItems: Integer; var ServerHandles: PSafeArray; out Errors: PSafeArray;
                        TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray;
                         out Errors: PSafeArray; TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncCancel(CancelID: Integer); safecall;
    property Parent: IOPCAutoServer read Get_Parent;
    property Name: WideString read Get_Name write Set_Name;
    property IsPublic: WordBool read Get_IsPublic;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property IsSubscribed: WordBool read Get_IsSubscribed write Set_IsSubscribed;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property ServerHandle: Integer read Get_ServerHandle;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property TimeBias: Integer read Get_TimeBias write Set_TimeBias;
    property DeadBand: Single read Get_DeadBand write Set_DeadBand;
    property UpdateRate: Integer read Get_UpdateRate write Set_UpdateRate;
    property OPCItems: OPCItems read Get_OPCItems;
  end;

// *********************************************************************//
// DispIntf:  IOPCGroupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F96-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroupDisp = dispinterface
    ['{28E68F96-8D75-11D1-8DC3-3C302A000000}']
    property Parent: IOPCAutoServer readonly dispid 1610743808;
    property Name: WideString dispid 1610743809;
    property IsPublic: WordBool readonly dispid 1610743811;
    property IsActive: WordBool dispid 1610743812;
    property IsSubscribed: WordBool dispid 1610743814;
    property ClientHandle: Integer dispid 1610743816;
    property ServerHandle: Integer readonly dispid 1610743818;
    property LocaleID: Integer dispid 1610743819;
    property TimeBias: Integer dispid 1610743821;
    property DeadBand: Single dispid 1610743823;
    property UpdateRate: Integer dispid 1610743825;
    property OPCItems: OPCItems readonly dispid 0;
    procedure SyncRead(Source: Smallint; NumItems: Integer;
                       var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       out Values: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant; out Qualities: OleVariant;
                       out TimeStamps: OleVariant); dispid 1610743828;
    procedure SyncWrite(NumItems: Integer; var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                        var Values: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                        out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743829;
    procedure AsyncRead(NumItems: Integer; var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                        out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant; TransactionID: Integer;
                        out CancelID: Integer); dispid 1610743830;
    procedure AsyncWrite(NumItems: Integer; var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                         var Values: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                         out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant; TransactionID: Integer;
                         out CancelID: Integer); dispid 1610743831;
    procedure AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer); dispid 1610743832;
    procedure AsyncCancel(CancelID: Integer); dispid 1610743833;
  end;

// *********************************************************************//
// DispIntf:  DIOPCGroupEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F97-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCGroupEvent = dispinterface
    ['{28E68F97-8D75-11D1-8DC3-3C302A000000}']
    procedure DataChange(TransactionID: Integer; NumItems: Integer;
                         var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                         var ItemValues: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                         var Qualities: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                         var TimeStamps: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1;
    procedure AsyncReadComplete(TransactionID: Integer; NumItems: Integer;
                                var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                var ItemValues: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                var Qualities: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                var TimeStamps: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                var Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 2;
    procedure AsyncWriteComplete(TransactionID: Integer; NumItems: Integer;
                                 var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                                 var Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 3;
    procedure AsyncCancelComplete(CancelID: Integer); dispid 4;
  end;

// *********************************************************************//
// Interface: OPCItems
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F98-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItems = interface(IDispatch)
    ['{28E68F98-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: OPCGroup; safecall;
    function Get_DefaultRequestedDataType: Smallint; safecall;
    procedure Set_DefaultRequestedDataType(DefaultRequestedDataType: Smallint); safecall;
    function Get_DefaultAccessPath: WideString; safecall;
    procedure Set_DefaultAccessPath(const DefaultAccessPath: WideString); safecall;
    function Get_DefaultIsActive: WordBool; safecall;
    procedure Set_DefaultIsActive(DefaultIsActive: WordBool); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): OPCItem; safecall;
    function GetOPCItem(ServerHandle: Integer): OPCItem; safecall;
    function AddItem(const ItemID: WideString; ClientHandle: Integer): OPCItem; safecall;
    procedure AddItems(NumItems: Integer; var ItemIDs: PSafeArray; var ClientHandles: PSafeArray;
                       out ServerHandles: PSafeArray; out Errors: PSafeArray;
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); safecall;
    procedure Remove(NumItems: Integer; var ServerHandles: PSafeArray; out Errors: PSafeArray); safecall;
    procedure Validate(NumItems: Integer; var ItemIDs: PSafeArray; out Errors: PSafeArray;
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); safecall;
    procedure SetActive(NumItems: Integer; var ServerHandles: PSafeArray; ActiveState: WordBool;
                        out Errors: PSafeArray); safecall;
    procedure SetClientHandles(NumItems: Integer; var ServerHandles: PSafeArray;
                               var ClientHandles: PSafeArray; out Errors: PSafeArray); safecall;
    procedure SetDataTypes(NumItems: Integer; var ServerHandles: PSafeArray;
                           var RequestedDataTypes: PSafeArray; out Errors: PSafeArray); safecall;
    property Parent: OPCGroup read Get_Parent;
    property DefaultRequestedDataType: Smallint read Get_DefaultRequestedDataType write Set_DefaultRequestedDataType;
    property DefaultAccessPath: WideString read Get_DefaultAccessPath write Set_DefaultAccessPath;
    property DefaultIsActive: WordBool read Get_DefaultIsActive write Set_DefaultIsActive;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  OPCItemsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F98-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItemsDisp = dispinterface
    ['{28E68F98-8D75-11D1-8DC3-3C302A000000}']
    property Parent: OPCGroup readonly dispid 1610743808;
    property DefaultRequestedDataType: Smallint dispid 1610743809;
    property DefaultAccessPath: WideString dispid 1610743811;
    property DefaultIsActive: WordBool dispid 1610743813;
    property Count: Integer readonly dispid 1610743815;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): OPCItem; dispid 0;
    function GetOPCItem(ServerHandle: Integer): OPCItem; dispid 1610743818;
    function AddItem(const ItemID: WideString; ClientHandle: Integer): OPCItem; dispid 1610743819;
    procedure AddItems(NumItems: Integer; var ItemIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       out ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); dispid 1610743820;
    procedure Remove(NumItems: Integer; var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                     out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743821;
    procedure Validate(NumItems: Integer; var ItemIDs: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); dispid 1610743822;
    procedure SetActive(NumItems: Integer; var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                        ActiveState: WordBool; out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743823;
    procedure SetClientHandles(NumItems: Integer;
                               var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                               var ClientHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                               out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743824;
    procedure SetDataTypes(NumItems: Integer;
                           var ServerHandles: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                           var RequestedDataTypes: {NOT_OLEAUTO(PSafeArray)}OleVariant;
                           out Errors: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743825;
  end;

// *********************************************************************//
// Interface: OPCItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F99-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItem = interface(IDispatch)
    ['{28E68F99-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: OPCGroup; safecall;
    function Get_ClientHandle: Integer; safecall;
    procedure Set_ClientHandle(ClientHandle: Integer); safecall;
    function Get_ServerHandle: Integer; safecall;
    function Get_AccessPath: WideString; safecall;
    function Get_AccessRights: Integer; safecall;
    function Get_ItemID: WideString; safecall;
    function Get_IsActive: WordBool; safecall;
    procedure Set_IsActive(IsActive: WordBool); safecall;
    function Get_RequestedDataType: Smallint; safecall;
    procedure Set_RequestedDataType(RequestedDataType: Smallint); safecall;
    function Get_Value: OleVariant; safecall;
    function Get_Quality: Integer; safecall;
    function Get_TimeStamp: TDateTime; safecall;
    function Get_CanonicalDataType: Smallint; safecall;
    function Get_EUType: Smallint; safecall;
    function Get_EUInfo: OleVariant; safecall;
    procedure Read(Source: Smallint; out Value: OleVariant; out Quality: OleVariant;
                   out TimeStamp: OleVariant); safecall;
    procedure Write(Value: OleVariant); safecall;
    property Parent: OPCGroup read Get_Parent;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property ServerHandle: Integer read Get_ServerHandle;
    property AccessPath: WideString read Get_AccessPath;
    property AccessRights: Integer read Get_AccessRights;
    property ItemID: WideString read Get_ItemID;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property RequestedDataType: Smallint read Get_RequestedDataType write Set_RequestedDataType;
    property Value: OleVariant read Get_Value;
    property Quality: Integer read Get_Quality;
    property TimeStamp: TDateTime read Get_TimeStamp;
    property CanonicalDataType: Smallint read Get_CanonicalDataType;
    property EUType: Smallint read Get_EUType;
    property EUInfo: OleVariant read Get_EUInfo;
  end;

// *********************************************************************//
// DispIntf:  OPCItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F99-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItemDisp = dispinterface
    ['{28E68F99-8D75-11D1-8DC3-3C302A000000}']
    property Parent: OPCGroup readonly dispid 1610743808;
    property ClientHandle: Integer dispid 1610743809;
    property ServerHandle: Integer readonly dispid 1610743811;
    property AccessPath: WideString readonly dispid 1610743812;
    property AccessRights: Integer readonly dispid 1610743813;
    property ItemID: WideString readonly dispid 1610743814;
    property IsActive: WordBool dispid 1610743815;
    property RequestedDataType: Smallint dispid 1610743817;
    property Value: OleVariant readonly dispid 0;
    property Quality: Integer readonly dispid 1610743820;
    property TimeStamp: TDateTime readonly dispid 1610743821;
    property CanonicalDataType: Smallint readonly dispid 1610743822;
    property EUType: Smallint readonly dispid 1610743823;
    property EUInfo: OleVariant readonly dispid 1610743824;
    procedure Read(Source: Smallint; out Value: OleVariant; out Quality: OleVariant;
                   out TimeStamp: OleVariant); dispid 1610743825;
    procedure Write(Value: OleVariant); dispid 1610743826;
  end;

// *********************************************************************//
// Interface: OPCBrowser
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F94-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCBrowser = interface(IDispatch)
    ['{28E68F94-8D75-11D1-8DC3-3C302A000000}']
    function Get_Organization: Integer; safecall;
    function Get_Filter: WideString; safecall;
    procedure Set_Filter(const Filter: WideString); safecall;
    function Get_DataType: Smallint; safecall;
    procedure Set_DataType(DataType: Smallint); safecall;
    function Get_AccessRights: Integer; safecall;
    procedure Set_AccessRights(AccessRights: Integer); safecall;
    function Get_CurrentPosition: WideString; safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): WideString; safecall;
    procedure ShowBranches; safecall;
    procedure ShowLeafs(Flat: OleVariant); safecall;
    procedure MoveUp; safecall;
    procedure MoveToRoot; safecall;
    procedure MoveDown(const Branch: WideString); safecall;
    procedure MoveTo(var Branches: PSafeArray); safecall;
    function GetItemID(const Leaf: WideString): WideString; safecall;
    function GetAccessPaths(const ItemID: WideString): OleVariant; safecall;
    property Organization: Integer read Get_Organization;
    property Filter: WideString read Get_Filter write Set_Filter;
    property DataType: Smallint read Get_DataType write Set_DataType;
    property AccessRights: Integer read Get_AccessRights write Set_AccessRights;
    property CurrentPosition: WideString read Get_CurrentPosition;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  OPCBrowserDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F94-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCBrowserDisp = dispinterface
    ['{28E68F94-8D75-11D1-8DC3-3C302A000000}']
    property Organization: Integer readonly dispid 1610743808;
    property Filter: WideString dispid 1610743809;
    property DataType: Smallint dispid 1610743811;
    property AccessRights: Integer dispid 1610743813;
    property CurrentPosition: WideString readonly dispid 1610743815;
    property Count: Integer readonly dispid 1610743816;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): WideString; dispid 1610743818;
    procedure ShowBranches; dispid 1610743819;
    procedure ShowLeafs(Flat: OleVariant); dispid 1610743820;
    procedure MoveUp; dispid 1610743821;
    procedure MoveToRoot; dispid 1610743822;
    procedure MoveDown(const Branch: WideString); dispid 1610743823;
    procedure MoveTo(var Branches: {NOT_OLEAUTO(PSafeArray)}OleVariant); dispid 1610743824;
    function GetItemID(const Leaf: WideString): WideString; dispid 1610743825;
    function GetAccessPaths(const ItemID: WideString): OleVariant; dispid 1610743826;
  end;

// *********************************************************************//
// DispIntf:  DIOPCServerEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F93-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCServerEvent = dispinterface
    ['{28E68F93-8D75-11D1-8DC3-3C302A000000}']
    procedure ServerShutDown(const Reason: WideString); dispid 1;
  end;

// *********************************************************************//
// Interface: IOPCActivator
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {860A4800-46A4-478B-A776-7F3A019369E3}
// *********************************************************************//
  IOPCActivator = interface(IDispatch)
    ['{860A4800-46A4-478B-A776-7F3A019369E3}']
    function Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IOPCActivatorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {860A4800-46A4-478B-A776-7F3A019369E3}
// *********************************************************************//
  IOPCActivatorDisp = dispinterface
    ['{860A4800-46A4-478B-A776-7F3A019369E3}']
    function Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer; dispid 1610743808;
  end;

// *********************************************************************//
// The Class CoOPCGroup provides a Create and CreateRemote method to
// create instances of the default interface IOPCGroup exposed by
// the CoClass OPCGroup. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoOPCGroup = class
    class function Create: IOPCGroup;
    class function CreateRemote(const MachineName: string): IOPCGroup;
  end;

// *********************************************************************//
// The Class CoOPCGroups provides a Create and CreateRemote method to
// create instances of the default interface IOPCGroups exposed by
// the CoClass OPCGroups. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoOPCGroups = class
    class function Create: IOPCGroups;
    class function CreateRemote(const MachineName: string): IOPCGroups;
  end;

// *********************************************************************//
// The Class CoOPCActivator provides a Create and CreateRemote method to
// create instances of the default interface IOPCActivator exposed by
// the CoClass OPCActivator. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoOPCActivator = class
    class function Create: IOPCActivator;
    class function CreateRemote(const MachineName: string): IOPCActivator;
  end;

// *********************************************************************//
// The Class CoOPCServer provides a Create and CreateRemote method to
// create instances of the default interface IOPCAutoServer exposed by
// the CoClass OPCServer. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoOPCServer = class
    class function Create: IOPCAutoServer;
    class function CreateRemote(const MachineName: string): IOPCAutoServer;
  end;

implementation

uses ComObj;

class function CoOPCGroup.Create: IOPCGroup;
begin
  Result := CreateComObject(CLASS_OPCGroup) as IOPCGroup;
end;

class function CoOPCGroup.CreateRemote(const MachineName: string): IOPCGroup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCGroup) as IOPCGroup;
end;

class function CoOPCGroups.Create: IOPCGroups;
begin
  Result := CreateComObject(CLASS_OPCGroups) as IOPCGroups;
end;

class function CoOPCGroups.CreateRemote(const MachineName: string): IOPCGroups;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCGroups) as IOPCGroups;
end;

class function CoOPCActivator.Create: IOPCActivator;
begin
  Result := CreateComObject(CLASS_OPCActivator) as IOPCActivator;
end;

class function CoOPCActivator.CreateRemote(const MachineName: string): IOPCActivator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCActivator) as IOPCActivator;
end;

class function CoOPCServer.Create: IOPCAutoServer;
begin
  Result := CreateComObject(CLASS_OPCServer) as IOPCAutoServer;
end;

class function CoOPCServer.CreateRemote(const MachineName: string): IOPCAutoServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCServer) as IOPCAutoServer;
end;

end.

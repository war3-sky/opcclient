program OPCquick;

{$IFDEF VER150}
{$WARN UNSAFE_CAST OFF}
{$WARN UNSAFE_TYPE OFF}
{$ENDIF}
{$IFDEF VER170}
{$WARN UNSAFE_CAST OFF}
{$WARN UNSAFE_CODE OFF}
{$ENDIF}

uses
  Variants, Windows, Classes, Forms, ComObj, ActiveX, SysUtils, OPCtypes, OPCDA, OPCutils,
  OPCAutomation_TLB, StrUtils, IniFiles,
  Redis.Commons, // Interfaces and types
  Redis.Client, // The client itself
  Redis.NetLib.INDY, // The tcp library used
  Redis.Values; // nullable types for redis commands

{$R *.RES}

const
  ServerProgID = 'Kepware.KEPServerEX.V5';//'MRD.DA2.1';//'Matrikon.OPC.Simulation';
  ConfigFileName = 'config.ini';

var
  ICount: Integer = 0;
  lRedis: TRedisClient;
  LastTime: Int64 = 0;

type
  //OPC2.0回调标准
  TOPCDataCallback = class(TInterfacedObject, IOPCDataCallback)
  public
    function OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
      pErrors: PResultList): HResult; stdcall;
    function OnCancelComplete(dwTransid: DWORD; hGroup: OPCHANDLE):
      HResult; stdcall;
  end;

var
  ServerIf: IOPCServer;
  GroupIf: IOPCItemMgt;
  GroupHandle: OPCHANDLE;
  ItemHandle: OPCHANDLE;
  ItemType: TVarType;
  ItemValue: string;
  ItemQuality: Word;
  ItemTime: TFileTime;
  HR: HResult;
  AsyncConnection: Longint;
  OPCDataCallback: IOPCDataCallback;
  ItemList: TStrings;
  I: Integer;
  UTCBias: Int64;

function GetUTCBias: LongInt;
var
  pTime: _TIME_ZONE_INFORMATION;
begin
  GetTimeZoneInformation(pTime);//获取时区
  Result := pTime.Bias * 60000 * -1; //pTime.Bias单位为分钟
end;

function DelphiTime2CommonTime(AValue: TDateTime): Int64;
begin
  //将时间转换未标准时间，此处时间取到毫秒级
  Result := Trunc(AValue * 24 * 60 * 60) * 1000;
   //标准UTC起始时间为北京时间 1970-1-1 08:00:00 000, delphi 起始日期为1899-12-30 00:00:00 000
  Result := Result - 2209161600000 - UTCBias;
end;

function ReadConfigProperties(const Section, Key, Default: string): string;
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0))+'config.ini');
  try
    Result := IniFile.ReadString(Section, Key, Default);
    if Result = '' then
      Result := Default;
  finally
    IniFile.Free;
  end;
end;

procedure Reconnect;
begin
  try
    lredis.Disconnect;
    lredis.Connect;
  except
    on E: Exception do
    begin
      Writeln('重连失败！', E.Message);
      Reconnect();
    end;
  end;
end;

procedure WriteTagValue(TagName, TagValue : String; Time: TDateTime);  overload;
begin
  try
    lRedis.&SET(TagName, '"{\"value\":\"' + TagValue+'\", \"time\":'+IntToStr(DelphiTime2CommonTime(Time))+'}"');
  except
    on E: Exception do
    begin
      Writeln('重连Reids！', E.Message);
      Reconnect();
    end;
  end;
end;

function InternalFileTimeToDateTime(Time: TFileTime): TDateTime;

  function InternalEncodeDateTime(const AYear, AMonth, ADay, AHour, AMinute, ASecond,
    AMilliSecond: Word): TDateTime;
  var
    LTime: TDateTime;
    Success: Boolean;
  begin
    Result := 0;
    Success := TryEncodeDate(AYear, AMonth, ADay, Result);
    if Success then
    begin
      Success := TryEncodeTime(AHour, AMinute, ASecond, AMilliSecond, LTime);
      if Success then
        if Result >= 0 then
          Result := Result + LTime
        else
          Result := Result - LTime
    end;
  end;
var
  LFileTime: TFileTime;
  SysTime: TSystemTime;
begin
  Result := 0;
  FileTimeToLocalFileTime(Time, LFileTime);

  if FileTimeToSystemTime(LFileTime, SysTime) then
    Result := InternalEncodeDateTime(SysTime.wYear, SysTime.wMonth, SysTime.wDay,
      SysTime.wHour, SysTime.wMinute, SysTime.wSecond, SysTime.wMilliseconds);
end;

{TOPCDataCallback}

function TOPCDataCallback.OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
var
  ClientItems: POPCHANDLEARRAY;
  Values: POleVariantArray;
  Qualities: PWORDARRAY;
  Times: PFileTimeArray;
  I, iSuccess, iFail: Integer;
  NewValue: string;
  TagName: string;
  start: Int64;
begin
  Result := S_OK;
  LastTime := GetTickCount;
  ClientItems := POPCHANDLEARRAY(phClientItems);
  Values := POleVariantArray(pvValues);
  Qualities := PWORDARRAY(pwQualities);
  Times := PFileTimeArray(pftTimeStamps);
  start := GetTickCount;
  iFail := 0;
  iSuccess := 0;
  for I := 0 to dwCount - 1 do
  begin
    TagName := ItemList[ClientItems[I]];
    if TVarData(Values[I]).VType > varTypeMask then
      continue;
    NewValue := VarToStr(Values[I]);
    if Qualities[I] = OPC_QUALITY_GOOD then
    begin
      WriteTagValue(TagName, NewValue, InternalFileTimeToDateTime(Times[I]));
      Inc(iSuccess);
    end
    else begin
      Inc(iFail);
      Writeln(DateTimeToStr(now), '数据点 ', TagName,' 读取失败！质量不佳, 值为：', NewValue);
    end;
  end;
  Writeln(DateTimeToStr(now), '总点数：', dwCount, '写入成功：', iSuccess, ' 写入失败：', iFail, ', 耗时：', GetTickCount - start, '毫秒');
end;

function TOPCDataCallback.OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
begin
  LastTime := GetTickCount;
  Result := OnDataChange(dwTransid, hGroup, hrMasterquality, hrMastererror,
    dwCount, phClientItems, pvValues, pwQualities, pftTimeStamps, pErrors);
end;

function TOPCDataCallback.OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
  pErrors: PResultList): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;

function TOPCDataCallback.OnCancelComplete(dwTransid: DWORD;
  hGroup: OPCHANDLE): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;

procedure ReadAllItemsFromOPCServer;
var
  Browser: OPCBrowser;
  I: Integer;
  OPCServer: IOPCAutoServer;
begin
  OPCServer := CoOPCServer.Create;
  OPCServer.Connect(ServerProgID, OPCServer.Get_ServerNode);
  Browser := OPCServer.CreateBrowser;
  Browser.ShowLeafs(True);
  ItemList := TStringList.Create;
  for I := 1 to Browser.Count do
  begin
    ItemList.Add(Browser.Item(I));
  end;
end;                             

// main program code

var
  RedisHost: string;
  RedisPort: Integer;
  OpcServerName: string;
begin
  Application.Initialize;
  Writeln('=====================================================================');
  Writeln('格物数据采集程序 @Copyright 苏州格物信息技术有限公司2017-2030.');
  Writeln('=====================================================================');
  Writeln;
  //设置时区偏差
  UTCBias := GetUTCBias;
  try
    //链接redis
    RedisHost := ReadConfigProperties('Redis', 'Host', '127.0.0.1');
    RedisPort := StrToInt(ReadConfigProperties('Redis', 'Port', '6379'));
    lRedis := TRedisClient.Create(RedisHost, RedisPort);
    lRedis.Connect;
  except
    on E: Exception do
    begin
      WriteLn(E.ClassName, ': ', E.Message);
      Reconnect();
    end;
  end;
  while True do
  begin
    if ICount > 0 then
    begin
      Sleep(3000);
      Writeln('第', ICount, '重新初始化OPC客户端！');
    end;
    try
      OpcServerName := ReadConfigProperties('OPC', 'ServerProgID', ServerProgID);
      ServerIf := CreateComObject(ProgIDToClassID(ServerProgID)) as IOPCServer;
    except
      ServerIf := nil;
    end;
    if ServerIf <> nil then
    begin
      Writeln('连接OPC服务',ServerProgID, '成功！');
    end
    else
    begin
      Writeln('连接OPC服务',ServerProgID, '失败！');
      Inc(ICount);
      continue;
    end;

    //获取所有点信息
    ReadAllItemsFromOPCServer;

    //添加组信息
    HR := ServerAddGroup(ServerIf, 'OPCGROUP', True, 100, 0, GroupIf, GroupHandle);
    if Succeeded(HR) then
    begin
      Writeln('添加组', 'OPCGROUP', '成功！');
    end
    else begin
      Writeln('添加组', 'OPCGROUP', '失败！');
      Inc(ICount);
      continue;
    end;

    //数据点添加到组同时读取最新值
    for I := 0 to ItemList.Count - 1 do
    begin
      HR := GroupAddItem(GroupIf, ItemList[I], I, VT_EMPTY, ItemHandle, ItemType);
      if Succeeded(HR) then
      begin
        Writeln('添加点 ', ItemList[I], ' 到组中！', I, '/', ItemList.Count);
        //读取点的值
        try
          HR := ReadOPCGroupItemValue(GroupIf, ItemHandle, ItemValue, ItemQuality, ItemTime);
          if Succeeded(HR) then
          begin
            if (ItemQuality and OPC_QUALITY_MASK) = OPC_QUALITY_GOOD then
            begin
              Writeln('数据点 ', ItemList[I], ' 同步读取 ', ItemValue);
              WriteTagValue(ItemList[I], ItemValue, InternalFileTimeToDateTime(ItemTime));
            end
            else begin
              Writeln('数据点 ', ItemList[I], ' 同步读取成功，但是质量不佳，值为：', ItemValue);
            end;
          end
          else begin
            Writeln('数据点 ', ItemList[I], ' 同步读取失败！');
          end;
        except
           Writeln('数据点', ItemList[I], ' 同步读取错误！');
        end;
        //读取点值结束
      end
      else begin
        Writeln('添加点 ', ItemList[I], ' 到组'+ ItemList[I] +'中失败！', I, '/', ItemList.Count );
        ServerIf.RemoveGroup(GroupHandle, True);
        continue;
      end;
    end;
    //添加回调
    OPCDataCallback := TOPCDataCallback.Create;
    try
      HR := GroupAdvise2(GroupIf, OPCDataCallback, AsyncConnection);
      if Failed(HR) then
      begin
        Writeln('添加回调监听失败！');
        Inc(ICount);
      end
      else begin
        lastTime := GetTickCount;
        ICount := 0;
        while GetTickCount - LastTime < 10000 do
        begin
          //每间隔30秒，将存活信息写入redis中，供第三方程序对本程序进行监测
          Application.ProcessMessages;
        end;
        Inc(ICount);
      end;
    finally
      GroupUnadvise2(GroupIf, AsyncConnection);
    end;
  end;
end.